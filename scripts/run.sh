#!/bin/bash
docker run --gpus all -it --rm -e DISPLAY="$DISPLAY" \
    -v /tmp/.X11-unix:/tmp/.X11-unix:rw -v $PWD:/data \
    --network tensorflow:local \
    bash