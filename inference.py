import cv2
import numpy as np
import tensorflow as tf
import PIL.Image as Image
import threading
import time
from audioplayer import AudioPlayer


IMAGE_SHAPE = (224, 224)
class_names = ['auto', 'circle', 'cross', 'hand', 'lion', 'rectangle', 'square', 'star','tourtle', 'triangle']


def load_model():
    return tf.keras.models.load_model("figures_model")


def predict(path, model):
    res_img = Image.open(path).resize(IMAGE_SHAPE)
    res_img = np.array(res_img) / 255.0
    result = model.predict(res_img[np.newaxis, ...])
    predicted_id = np.argmax(result, axis=-1)
    predicted_label = class_names[predicted_id[0]]
    print(predicted_label.title(), np.max(result))
    return predicted_label.title(), np.max(result)


def predict_img(model, img):
    res_img = np.array(img) / 255.0
    result = model.predict(res_img[np.newaxis, ...])
    predicted_id = np.argmax(result, axis=-1)
    predicted_label = class_names[predicted_id[0]]
    return predicted_label.title(), np.max(result)


def thread_function(name):
    time.sleep(1)
    print('Play sound:', name.lower())
    if name.lower() == 'circle':
        AudioPlayer("sounds/der_kreis.mp3").play(block=True)
    elif name.lower() == 'cross':
        AudioPlayer("sounds/das_kreuz.mp3").play(block=True)
    elif name.lower() == 'square':
        AudioPlayer("sounds/das_quadrat.mp3").play(block=True)
    elif name.lower() == 'star':
        AudioPlayer("sounds/der_stern.mp3").play(block=True)
    elif name.lower() == 'triangle':
        AudioPlayer("sounds/das_dreieck.mp3").play(block=True)
    elif name.lower() == 'auto':
        AudioPlayer("sounds/das_auto.mp3").play(block=True)
    elif name.lower() == 'rectangle':
        AudioPlayer("sounds/das_rechteck.mp3").play(block=True)
    elif name.lower() == 'lion':
        AudioPlayer("sounds/der_lowe.mp3").play(block=True)
    elif name.lower() == 'tourtle':
        AudioPlayer("sounds/die_schildkrote.mp3").play(block=True)
    elif name.lower() == 'hand':
        AudioPlayer("sounds/die_hand.mp3").play(block=True)
    time.sleep(1)
    print("done playing")


if __name__ == '__main__':
    model_reconstructed = load_model()
    cap = cv2.VideoCapture(1)
    frames = 0
    t = threading.Thread(target=thread_function, args=("",))
    alive = False
    lock = threading.Lock()
    while True:
        try:
            t1 = time.time()
            ret, frame = cap.read()
            if frames % 2 == 0:
                cv2.imshow('frame', frame)
                frame = cv2.resize(frame, dsize=IMAGE_SHAPE, interpolation=cv2.INTER_CUBIC)
                color_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                label, score = predict_img(model_reconstructed, color_frame)
                if score > 1.4:
                    with lock:
                        alive = t.is_alive()
                    if not alive:
                        t = threading.Thread(target=thread_function, args=(label,))
                        t.start()
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
            t2 = time.time()
            s = f"""{int(1 / (t2 - t1))} FPS"""
            frames = frames + 1
        except KeyboardInterrupt:
            cap.release()
            cv2.destroyAllWindows()
