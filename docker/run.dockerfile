FROM nvcr.io/nvidia/l4t-tensorflow:r32.5.0-tf2.3-py3
RUN apt-get update && apt-get install -yqq python3-opencv python3-gi python3-gst-1.0 && pip3 install audioplayer pillow