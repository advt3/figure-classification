FROM tensorflow/tensorflow:latest-gpu
RUN /usr/bin/python3 -m pip install --upgrade pip && pip3 install tensorflow-hub jupyterlab matplotlib
ARG USER_ID=1000
ARG GROUP_ID=1000
WORKDIR /home/training
RUN groupadd -g ${GROUP_ID} training
RUN useradd -l -u ${USER_ID} training -g training
RUN chown -R ${USER_ID}:${GROUP_ID} /home/training
USER training
CMD jupyter-lab --ip 0.0.0.0
