# Figure Classification
Learning languages using image classification

On this repository we prepare a classifier and inference for basic shapes that says the word in german to reinforce kids learning.

![img](IMG_5906.jpg)

## Usage

- Train the network with the [classification](classification.ipynb) notebook
  - the figures folder should contain a folder for each figure ['circle', 'cross', 'square', 'star', 'triangle']
- Run the inference.py python file to run on the device

## LICENSE
MIT License Copyright (c) 2021 Santiago Hurtado


