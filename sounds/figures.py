from gtts import gTTS


def create_sounds():
    tts = gTTS('das Quadrat', lang='de')
    tts.save('das_quadrat.mp3')

    tts = gTTS('das Dreieck', lang='de')
    tts.save('das_dreieck.mp3')

    tts = gTTS('der Kreis', lang='de')
    tts.save('der_kreis.mp3')

    tts = gTTS('das Kreuz', lang='de')
    tts.save('das_kreuz.mp3')

    tts = gTTS('der Stern', lang='de')
    tts.save('der_stern.mp3')

    tts = gTTS('das Auto', lang='de')
    tts.save('das_auto.mp3')

    tts = gTTS('das Rechteck', lang='de')
    tts.save('das_rechteck.mp3')

    tts = gTTS('die Hand', lang='de')
    tts.save('die_hand.mp3')

    tts = gTTS('der Löwe', lang='de')
    tts.save('der_lowe.mp3')

    tts = gTTS('die Schildkröte', lang='de')
    tts.save('die_schildkrote.mp3')


if __name__ == "__main__":
    create_sounds()
