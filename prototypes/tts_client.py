from gtts import gTTS
tts = gTTS('das Quadrat',lang='de')
tts.save('das_quadrat.mp3')

tts = gTTS('das Dreieck',lang='de')
tts.save('das_dreieck.mp3')

tts = gTTS('der Kreis',lang='de')
tts.save('der_kreis.mp3')

tts = gTTS('das Kreuz',lang='de')
tts.save('das_kreuz.mp3')
tts = gTTS('der Stern',lang='de')
tts.save('der_stern.mp3')

from audioplayer import AudioPlayer
# Playback stops when the object is destroyed (GC'ed), so save a reference to the object for non-blocking playback.
AudioPlayer("../sounds/das_quadrat.mp3").play(block=True)
AudioPlayer("../sounds/das_dreieck.mp3").play(block=True)
AudioPlayer("../sounds/das_kreuz.mp3").play(block=True)
AudioPlayer("../sounds/der_stern.mp3").play(block=True)
AudioPlayer("../sounds/der_kreis.mp3").play(block=True)
